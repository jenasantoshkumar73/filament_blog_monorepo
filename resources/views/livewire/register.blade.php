<div class="min-h-screen relative overflow-hidden flex bg-gradient-to-b from-blue-50 to-blue-400">
    <div class="z-10 flex-1 w-full max-w-lg py-8 bg-white md:py-16">
        <div class="w-full max-w-md px-4 mx-auto sm:px-6 md:px-8">
            <h1 class="text-xl font-semibold tracking-tight md:text-2xl">Create an account</h1>

            <form wire:submit.prevent="register" class="mt-8 space-y-6 md:mt-12 ">
                {{ $this->form }}
                <button class="flex items-center justify-center w-full h-8 px-3 text-sm font-semibold" type="submit">Get started</button>
            </form>

        </div>
    </div>
</div>