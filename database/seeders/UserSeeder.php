<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'Super Admin User',
            'email' => 'superadmin@test.com',
            'password'=>bcrypt('1234')
        ]);

        User::factory()->create([
            'name' => 'Admin User',
            'email' => 'admin@test.com',
            'password'=>bcrypt('1234')
        ]);
    }
}
