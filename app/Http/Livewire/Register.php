<?php

namespace App\Http\Livewire;

use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Register extends Component implements HasForms
{
    use InteractsWithForms;
    public function render(): View
    {
        return view('livewire.register');
    }
}
